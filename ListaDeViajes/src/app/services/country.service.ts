import { IRegions } from './../interfaces/iregions.interface';
import { ICountry } from './../interfaces/icountry.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CountryService {

    constructor(private http: HttpClient) { }



    getCountriesByRegion(region: string): Observable<ICountry[]> {
        return this.http.get<ICountry[]>('https://restcountries.com/v2/regionalbloc/' + region).pipe(map(data => data));
    }

    getAllRegions(): Observable<IRegions[]>{
        return this.http.get<IRegions[]>('assets/data/regions.json').pipe(map(data => data));

    }
}
