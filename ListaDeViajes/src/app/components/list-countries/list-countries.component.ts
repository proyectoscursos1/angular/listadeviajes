import { IRegions } from './../../interfaces/iregions.interface';
import { CountryService } from './../../services/country.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { ICountry } from 'src/app/interfaces/icountry.interface';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import  * as _ from 'lodash'; //npm i --save-dev @types/lodash --force

@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styleUrls: ['./list-countries.component.css']
})
export class ListCountriesComponent implements OnInit {

  listRegions: IRegions[] = [];
  listCountries: ICountry[] = [];
  listCountriesToVisit: ICountry[] = [];
  regionSelected: string;

  public load: boolean;
  public markers: any[];
  public zoom: number;

  constructor(private countryService: CountryService) {
    this.load = false;
    this.regionSelected = 'EU';
    this.markers = [];
    this.zoom = 2;
  }

  ngOnInit() {
    forkJoin( // Implementar 2 metodos a la par
      this.countryService.getCountriesByRegion("eu"),
      this.countryService.getAllRegions()
    ).subscribe(list => {

      this.listCountries = list[0];
      this.listRegions = list[1];

      this.load = true;
      console.log(this.listCountries);
      console.log(this.listRegions);
    }, error => {
      console.error('Error: ' + error);
      this.load = true;
    });
  }

  filterCountries($event: any) {
    this.load = false
    this.countryService.getCountriesByRegion($event.value).subscribe(list => {
      this.listCountries = _.differenceBy(list, this.listCountriesToVisit, (c: { name: any; }) => c.name); //Aplicacion de lodash para _.diferrenceBy
      this.load = true;
    });
  }

  drop(event: CdkDragDrop<ICountry[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

        if(event.container.id === 'visit'){
          //añadir marcador
          let c = event.container.data[event.currentIndex];
          this.markers.push({
            position: {
              lat: c.latlng[0],
              lng: c.latlng[1],
            },
            label: {
              color: 'black',
              text: c.name
            }
          });
        } else {
          //Eliminar marcador
          let c = event.container.data[event.currentIndex];
          let index = this.markers.findIndex(m=> m.label.text === c.name);
          this.markers.splice(index, 1)
        }
    }
  }
  }