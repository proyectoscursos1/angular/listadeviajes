export interface IRegions {
    code: string,
    name: string
}
